package com.onlinerocker.main;

import java.util.Scanner;

public class Main {
	
	/*
	 * Values:
	 * number of x's alone in a row
	 * number of o's alone in a row
	 * number of double x's
	 * number of double o's
	 * number of 3 x's
	 * number of 3 o's
	 * 
	 */
	
	public static void main(String args[]){
		Board board = new Board();
		AI ai = new AI(0.5, board);
		
		int AIWins = 0;
		int randomWins = 0;
		int draws = 0;
		
		for(int x=0; x<10000; x++){
			
			while(!board.isBoardDone()){
				ai.randomMove();
				board.logBoardInHistory(board.getBoard());
				board.displayBoard();
				
				if(board.isBoardDone()){
					
					if(board.getWinner() == 1){
						AIWins++;
					}else if(board.getWinner() == 2){
						randomWins++;
					}else{
						draws++;
					}
					
					break;
				}
				
				System.out.println();
				
				ai.chooseMove();
				board.logBoardInHistory(board.getBoard());
				board.displayBoard();
				
				if(board.isBoardDone()){
					
					if(board.getWinner() == 1){
						AIWins++;
					}else if(board.getWinner() == 2){
						randomWins++;
					}else{
						draws++;
					}
					
					break;
				}
				
				System.out.println();
			}
			
			for(int y=0; y<board.getBoardHistory().size(); y++){
				if(y+2<(board.getBoardHistory().size()-1)){
					ai.updateWeights(board.getBoardHistory().get(y), board.getBoardHistory().get(y+2));
				}else{
					ai.updateWeights(board.getBoardHistory().get(y), null);
				}
			}
			
			board.clearBoard();
			board.clearBoardHistory();
			
		}
		
		System.out.println();
		System.out.println();
		System.out.println("AI Wins: " + AIWins);
		System.out.println("Random Wins: " + randomWins);
		System.out.println("Draws: " + draws);
		System.out.println();
		
		Scanner scanner = new Scanner(System.in);
		
		while(!board.isBoardDone()){
			System.out.println("Choose x pos:");
			int x = scanner.nextInt();
			System.out.println("Choose y pos:");
			int y = scanner.nextInt();
			
			while(!board.setSpace(x, y, 2)){
				System.out.println("Choose x pos:");
				x = scanner.nextInt();
				System.out.println("Choose y pos:");
				y = scanner.nextInt();
			}
				
			board.displayBoard();
			System.out.println();
			
			if(board.isBoardDone()){
				break;
			}
			
			ai.chooseMove();
			board.displayBoard();
			System.out.println();
		}
		
		if(board.getWinner() == 1){
			System.out.println("YOU LOSE!");
		}else if(board.getWinner() == 2){
			System.out.println("YOU WIN!");
		}else{
			System.out.println("DRAW");
		}
		
		scanner.close();
		
	}

}
