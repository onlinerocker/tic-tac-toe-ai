package com.onlinerocker.main;

import java.util.ArrayList;
import java.util.Random;

public class AI {

	private double[] weights;
	private Board board;
	
	public AI(double weightDefault, Board board){
		this.board = board;
		
		weights = new double[7];
		
		for(int x=0; x<7; x++){
			weights[x] = weightDefault;
		}
	}
	
	public void chooseMove(){
		ArrayList<int[][]> successors = board.getSuccessors(1);
		
		int[][] bestSuccessor = successors.get(0);
		double bestVal = board.estimateValueOfBoard(weights, board.getValuesOfBoard(bestSuccessor));
		
		for(int[][] suc : successors){
			double val = board.estimateValueOfBoard(weights, board.getValuesOfBoard(suc));
			if(val > bestVal){
				bestVal = val;
				bestSuccessor = suc;
			}
		}
		
		board.setBoard(bestSuccessor);
	}
	
	public void randomMove(){
		ArrayList<int[][]> successors = board.getSuccessors(2);
		Random ran = new Random();
		
		int index = ran.nextInt(successors.size());
		
		board.setBoard(successors.get(index));
	}
	
	public void updateWeights(int[][] board1, int[][] board2){
		for(int x=0; x<7; x++){
			double[] values = board.getValuesOfBoard(board1);
			double[] valSuc = null;
			
			if(board2 != null){
				valSuc = board.getValuesOfBoard(board2);
			}
			
			if(x!=0){
				weights[x] = weights[x] + 0.1*(board.valueOfBoard(weights, values, valSuc) - board.estimateValueOfBoard(weights, values))*values[x-1];
			}else{
				weights[x] = weights[x] + 0.1*(board.valueOfBoard(weights, values, valSuc) - board.estimateValueOfBoard(weights, values));
			}
		}
	}
	
}
