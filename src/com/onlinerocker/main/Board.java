package com.onlinerocker.main;

import java.util.ArrayList;

public class Board {
	
	/*
	 * Values:
	 * value[0] = number of x's alone in a row
	 * 1 = number of o's alone in a row
	 * 2 = number of double x's
	 * 3 = number of double o's
	 * 4 = number of 3 x's
	 * 5 = number of 3 o's
	 * 
	 */
	
	private double[] values;
	private int[][] board;
	private ArrayList<int[][]> boardHistory;
	
	public Board(){
		values = new double[6];
		board = new int[3][3];
		boardHistory = new ArrayList<int[][]>();
		
		for(int x=0; x<6; x++){
			values[x] = 0;
		}
		
		for(int x=0; x<3; x++){       
			for(int y=0; y<3; y++){
				board[x][y] = 0;
			}
		}
		
	}
	
	public ArrayList<int[][]> getSuccessors(int target){
		ArrayList<int[][]> successors = new ArrayList<int[][]>();
		int[][] successor = board;
		

		
		for(int x=0; x<3; x++){
			for(int y=0; y<3; y++){
				if(board[x][y] == 0){
					successor[x][y] = target;
					int[][] copy = new int[3][3];
					
					for(int w=0; w<3; w++){
						for(int z=0; z<3; z++){
							copy[w][z] = successor[w][z];
						}
					}
					
					successors.add(copy);
					successor[x][y] = 0;
				}
			}
		}
		
		return successors;
	}
	
	public ArrayList<int[][]> getBoardHistory(){
		return boardHistory;
	}
	
	public void clearBoardHistory(){
		boardHistory.clear();
	}
	
	public void logBoardInHistory(int[][] board){
		boardHistory.add(board);
	}
	
	public void clearBoard(){
		for(int x=0; x<3; x++){
			for(int y=0; y<3; y++){
				board[x][y] = 0;
			}
		}
	}
	
	public double estimateValueOfBoard(double[] weights, double[] values1){
		double valueOfBoard = 0;
		
		for(int x=0; x<7; x++){
			if(x>=1){
				valueOfBoard += weights[x]*values1[x-1];
			}else{
				valueOfBoard += weights[x];
			}
		}
		
		return valueOfBoard;
	}
	
	public double valueOfBoard(double[] weights, double [] values1, double[] valuesOfSuccessor){
		double value = 0;
		
		if(values1[4] > 0){
			value = 100;
		}else if(values1[5] > 0){
			value = -100;
		}else if(isBoardDone()){
			value = 0;
		}else{
			value = estimateValueOfBoard(weights, valuesOfSuccessor);
		}
		
		return value;
	}
	
	public int getWinner(){
		int winner = 0;
		
		for(int x=0; x<3; x++){
			
			if(getAmountInSequence(getRow(board, x), 1) == 3 
					|| getAmountInSequence(getColumn(board, x), 1) == 3){
				winner = 1;
				break;
			}
			
			if(getAmountInSequence(getRow(board, x), 2) == 3 
					|| getAmountInSequence(getColumn(board, x), 2) == 3){
				winner = 2;
				break;
			}
			
		}
		
		if(getAmountInSequence(getDiagonal(board, true), 1) == 3
				|| getAmountInSequence(getDiagonal(board, false), 1) == 3){
			winner = 1;
		}
		
		if(getAmountInSequence(getDiagonal(board, true), 2) == 3
				|| getAmountInSequence(getDiagonal(board, false), 2) == 3){
			winner = 2;
		}
		
		return winner;
	}
	
	public boolean setSpace(int x, int y, int val){
		if(board[x][y] == 0){
			board[x][y] = val;
			return true;
		}else{
			System.out.println("Space taken!");
			return false;
		}		
	}
	
	public boolean isBoardDone(){
		boolean isDone = true;
		
		for(int x=0; x<3; x++){
			
			for(int y=0; y<3; y++){
				if(board[x][y] == 0){
					isDone = false;
				}
			}
			
			if(getAmountInSequence(getRow(board, x), 1) == 3 
					|| getAmountInSequence(getColumn(board, x), 1) == 3
					|| getAmountInSequence(getRow(board, x), 2) == 3 
					|| getAmountInSequence(getColumn(board, x), 2) == 3
					){
				isDone = true;
				break;
			}
		}
		
		if(getAmountInSequence(getDiagonal(board, true), 1) == 3
					|| getAmountInSequence(getDiagonal(board, false), 1) == 3
					|| getAmountInSequence(getDiagonal(board, true), 2) == 3
					|| getAmountInSequence(getDiagonal(board, false), 2) == 3){
			isDone = true;
		}
		
		return isDone;
	}
	
	public void displayBoard(){
		for(int x=0; x<3; x++){
			System.out.println();
			for(int y=0; y<3; y++){
				System.out.print(board[x][y]);
			}
		}
	}
	
	public void setBoard(int[][] board){
		this.board = board;
	}
	
	public int[][] getBoard(){
		return board;
	}
	
	public double[] getValuesOfBoard(int[][] board){
		/*
		 * Values:
		 * value[0] = number of x's alone in a row
		 * 1 = number of o's alone in a row
		 * 2 = number of double x's
		 * 3 = number of double o's
		 * 4 = number of 3 x's
		 * 5 = number of 3 o's
		 * 
		 * x's will be 1
		 * o's will be 2
		 */
		
		double[] values1 = new double[6];
		
		int val0Count = 0;
		int val1Count = 0;
		int val2Count = 0;
		int val3Count = 0;
		int val4Count = 0;
		int val5Count = 0;
		
		//need to add diagonals 
		
		for(int x=0; x<3; x++){
			if(getAmountInSequence(getRow(board, x), 1) == 1 && getAmountInSequence(getRow(board, x), 2) == 0){
				val0Count++;
			}
			if(getAmountInSequence(getColumn(board, x), 1) == 1 && getAmountInSequence(getColumn(board, x), 2) == 0){
				val0Count++;
			}
			
			if(getAmountInSequence(getRow(board, x), 2) == 1 && getAmountInSequence(getRow(board, x), 1) == 0){
				val1Count++;
			}
			if(getAmountInSequence(getColumn(board, x), 2) == 1 && getAmountInSequence(getColumn(board, x), 1) == 0){
				val1Count++;
			}
			
			if(getAmountInSequence(getRow(board, x), 1) == 2 && getAmountInSequence(getRow(board, x), 2) == 0){
				val2Count++;
			}
			if(getAmountInSequence(getColumn(board, x), 1) == 2 && getAmountInSequence(getColumn(board, x), 2) == 0){
				val2Count++;
			}
			
			if(getAmountInSequence(getRow(board, x), 2) == 2 && getAmountInSequence(getRow(board, x), 1) == 0){
				val3Count++;
			}
			if(getAmountInSequence(getColumn(board, x), 2) == 2 && getAmountInSequence(getColumn(board, x), 1) == 0){
				val3Count++;
			}
			
			if(getAmountInSequence(getRow(board, x), 1) == 3){
				val4Count++;
			}
			if(getAmountInSequence(getColumn(board, x), 1) == 3){
				val4Count++;
			}
			
			if(getAmountInSequence(getRow(board, x), 2) == 3){
				val5Count++;
			}
			if(getAmountInSequence(getColumn(board, x), 2) == 3){
				val5Count++;
			}
		}
		
		if(getAmountInSequence(getDiagonal(board, true), 1) == 1 && getAmountInSequence(getDiagonal(board, true), 2) == 0){
			val0Count++;
		}
		if(getAmountInSequence(getDiagonal(board, false), 1) == 1 && getAmountInSequence(getDiagonal(board, false), 2) == 0){
			val0Count++;
		}
		
		if(getAmountInSequence(getDiagonal(board, true), 2) == 1 && getAmountInSequence(getDiagonal(board, true), 1) == 0){
			val1Count++;
		}
		if(getAmountInSequence(getDiagonal(board, false), 2) == 1 && getAmountInSequence(getDiagonal(board, false), 1) == 0){
			val1Count++;
		}

		if(getAmountInSequence(getDiagonal(board, true), 1) == 2 && getAmountInSequence(getDiagonal(board, true), 2) == 0){
			val2Count++;
		}
		if(getAmountInSequence(getDiagonal(board, false), 1) == 2 && getAmountInSequence(getDiagonal(board, false), 2) == 0){
			val2Count++;
		}
		
		if(getAmountInSequence(getDiagonal(board, true), 2) == 2 && getAmountInSequence(getDiagonal(board, true), 1) == 0){
			val3Count++;
		}
		if(getAmountInSequence(getDiagonal(board, false), 2) == 2 && getAmountInSequence(getDiagonal(board, false), 1) == 0){
			val3Count++;
		}
		
		if(getAmountInSequence(getDiagonal(board, true), 1) == 3){
			val4Count++;
		}
		if(getAmountInSequence(getDiagonal(board, false), 1) == 3){
			val4Count++;
		}
		
		if(getAmountInSequence(getDiagonal(board, true), 2) == 3){
			val5Count++;
		}
		if(getAmountInSequence(getDiagonal(board, false), 2) == 3){
			val5Count++;
		}
		
		values1[0] = val0Count;
		values1[1] = val1Count;
		values1[2] = val2Count;
		values1[3] = val3Count;
		values1[4] = val4Count;
		values1[5] = val5Count;	
		
		return values1;
	}
	
	public int getAmountInSequence(int[] sequence, int target){
		int count = 0;
		
		for(int x=0; x<sequence.length; x++){
			if(sequence[x] == target){
				count++;
			}
		}
		
		return count;
	}
	
	public int[] getRow(int[][] board, int rowIndex){
		int[] row = new int[3];
		for(int x=0; x<3; x++){
			row[x] = board[rowIndex][x];
		}
		return row;
	}
	
	public int[] getColumn(int[][] board, int colIndex){
		int[] col = new int[3];
		for(int x=0; x<3; x++){
			col[x] = board[x][colIndex];
		}
		return col;
	}
	
	public int[] getDiagonal(int[][] board, boolean leftRight){
		int[] diag = new int[3];
		
		if(leftRight){
			diag[0] = board[0][0];
			diag[1] = board[1][1];
			diag[2] = board[2][2];
		}else{
			diag[0] = board[2][0];
			diag[1] = board[1][1];
			diag[2] = board[0][2];
		}
		
		return diag;
	}
	
	public double[] getValues(){
		return values;
	}
	
}
